from django.shortcuts import render

from django.contrib.auth.decorators import login_required, user_passes_test

def testa_professor(usuario):
    return usuario.tipo == 'P'

@login_required
@user_passes_test(testa_professor)
def home_professor(request):
    return render(request, 'restrito/home.html')
