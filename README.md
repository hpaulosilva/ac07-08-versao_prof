## Como Subir o App pela primeira vez

* instalem o django pelo pip assim como o django-widgets-tweaks
* apaguem o arquivo db.sqlite3
* rodem o comando "python manage.py loaddata db.json"
* rodem o comando "python manage.py runserver"

## Observações

Sempre que realizarem a inserção de dados no banco, rodar o dumpdata do manage.py somente para o nome do app que você está inserindo, suba o arquivo dump em formato json para o repositório e informe o restante da equipe para carregar os dados.